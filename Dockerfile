# Base Image
FROM node

# install and app dependencies
COPY package.json yarn.lock ./
ENV NODE_PATH=/node_modules
ENV PATH=$PATH:/node_modules/.bin
RUN yarn

# Add project files to /app route in Container
ADD . /app

# Set the working directory to /app
WORKDIR /app

# expose port 3000
EXPOSE 3000

# start app
CMD ["npm", "start"]