# Learning Cloud Hosting - Part 2

The aim of this project is to understand the basics of deploying a react app to the cloud.  
I'll use create react app to bootstrap the react app, as that's not the aim of the project.
I'll then deploy to the google app engine, via a docker container.

## Prerequisites

- Install the latest version of node.
- Install the latest version of yarn.
- Install docker.
- Install Google Cloud SDK.

## How to Install

1. Clone this repository to your machine from GitLab.
2. Install the project's dependencies:

```console
$ yarn
```

## How to Create the Docker Image of the React App

1. Create a docker image, using the instructions in the project's Dockerfile.

```console
$ docker image build -t myreactapp:test .
```

2. Then list your images to check it has been created:

```console
$ docker image ls
```

## How to Create a Container Instance from the Image Running Locally

1. The docker `run` command is used to create a container from the myreactapp:test image.  

```console
$ docker run -it -p 3000:3000 myreactapp:test /bin/bash  
```
(The -p (publish) flag publishes a port to the outside world.)

(By using the `-it` options above the container is interactive, so you can interact with the bash shell, once the contain has been created.)  

2. Start the react app from the container:  

```console
root@abcd123456:/app# yarn start
```

3. Show running containers, then stop and remove them:

```console
$ docker ps
$ docker stop [Container ID]
$ docker rm [Container ID]
```

## How to Run in a kubernetes Cluster locally

1. Install kubectl, the kubernetes command line tool.  
2. Install Minikube, which runs a single node kubernetes cluster locally.  
3. Start Minikube and create a cluster:  

```console
$ minikube start
```
4. Deploy our docker image.

```console
$ kubectl run myreactapp --image=myreactapp --port=80
```

5. Check the deployment:  

```console
$ kubectl get deployments
```

6. Delete the deployment:

```console
$ kubectl delete deployment myreactapp
```

## How to Push the Image to the Google Cloud

1. Create a Google Cloud project, via the GCP console.
2. Enable the Google Container Registry API, for that project.
3. Tag the image with a registry name using the pattern recommended by Google

```console
docker tag myreactapp gcr.io/[PROJECT-ID]/myreactapp
```

4. Push the image to the google cloud registry:

```console
docker push gcr.io/[PROJECT-ID]/myreactapp
```

## How to Run a Container in App Engine

1. Click 'create app', using europe-west2
2. 

## User Stories

As a developer,  
I want to create a docker image for my React app,  
So that I can create instances in future.  

As a developer,  
I want to create an instance of my app, in a container.  
So that I can run it via a docker container anywhere.

As a developer,  
I want to add my image to a registry in the cloud.  
So that I know how to push images to a remote repository.  

As a developer,  
I want to run an instance of my container in the cloud.  
So that I can run my app in the cloud.

As a developer,
I want to create a CI pipeline,
So that if I change my app it is automatically built and deployed.

## Resources

[Lawrence Tan Blog](https://medium.com/google-cloud/hosting-a-react-js-app-on-google-cloud-app-engine-6d1341b75d8c)  
[Michael Herman Blog](https://mherman.org/blog/dockerizing-a-react-app/)  
[Jose Ignacio Castelli Blog](https://medium.com/@joseignaciocastelli92/how-to-make-your-first-local-cluster-with-kubernetes-e15fc4b262e7)  
  
  
  
  
  
  
  -----------------------------------------------------------------
# VVVVV Bootstrapt Readme Below VVVVV

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
